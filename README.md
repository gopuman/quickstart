# Uffizzi Quickstart (~ 1 minute)

Go from merge request to Uffizzi ephemeral environment in less than one minute...

### 1. Fork this repo

From the project home page, select **Fork**, then choose a namespace and project slug. Select **Fork project**.

### 2. Ensure GitLab CI/CD is enabled for your project

If you don't see the **Build > Pipelines** option in left sidebar, following [these steps](https://docs.gitlab.com/ee/ci/enable_or_disable_ci.html#enable-cicd-in-a-project) to enable it.

### 3. Open a merge request (MR) for `try-uffizzi` branch against `master` in your fork

:warning: Be sure that you’re opening the MR on the branches of _your fork_ (i.e. `your-account/master` ← `your-account/try-uffizzi`).

If you try to open a MR for `uffizzi/quickstart/~/tree/master` ← `your-account/~/tree/try-uffizzi`, the pipeline will not run in this example.

That’s it! This will kick off a GitLab pipeline and the ephemeral environment URL will be dumped to `stdout` of the pipeline job. 


## What to expect

The MR will trigger a pipeline defined in [`.gitlab-ci.yml`](.gitlab-ci.yml) that creates a Uffizzi ephemeral environment for the [microservices application](#architecture-of-this-example-app) defined by this repo. The ephemeral environment URL will be echoed to `stdout` of the `deploy_environment` Job. Look for the following line in the Job logs:

```
$ echo "Uffizzi Environment deployment details at URI:${UFFIZZI_CONTAINERS_URI}"
Uffizzi Environment deployment details at URI:https://app.uffizzi.com//projects/8526/deployments/27720/containers
```

This link will take you to the Uffizzi Dashboard where you can view application logs and manage your environments and team. The environment will be deleted when the MR is merged/closed or after 1 hour ([configurable](https://gitlab.com/uffizzi/quickstart/-/blob/master/docker-compose.uffizzi.yml#L7)).

You might also want configure a new Job to post the URL as a comment to your MR issue or send a notification to Slack, MS Teams, etc. See our Slack notification example [here](https://gitlab.com/uffizzi/environment-action/-/blob/main/Notifications/slack.yml).

## How it works

### Configuration

Ephemeral environments are configured with a [Docker Compose template](docker-compose.uffizzi.yml) that describes the application components and a [GitLab CI pipeline](.gitlab-ci.yml) that includes a series of jobs triggered by a `merge_request_event`:

1. [Build and push images to a container registry](https://gitlab.com/uffizzi/quickstart/-/blob/master/.gitlab-ci.yml#L14)
2. [Render a Docker Compose file from the Docker Compose template and the built images](https://gitlab.com/uffizzi/quickstart/-/blob/master/.gitlab-ci.yml#L56-76)
3. [Deploy the application to a Uffizzi ephemeral environment and echo the environment URL to the Job logs](https://gitlab.com/uffizzi/environment-action/-/blob/main/environment.gitlab-ci.yml#L30-76)
4. [Delete the environment](https://gitlab.com/uffizzi/environment-action/-/blob/main/environment.gitlab-ci.yml#L98-115)

### Uffizzi Cloud

Running this workflow will create a [Uffizzi Cloud](https://uffizzi.com) account and project from your GitLab user and repo information, respectively. If you sign in to the [Uffizzi Dashboard](https://app.uffizzi.com/sign_in) you can view logs, password protect your environments, manage projects and team members, set role-based access controls, and configure single-sign on (SSO).

Our Starter Plan is free for up to two concurrent ephemeral environments. The Pro Plan gives your team access to unlimited environments. If you'd like to self-host Uffizzi on your own Kubernetes cluster, check out our Enterprise Plan. See [our pricing](https://uffizzi.com/pricing) for all plan details. 

## Acceptable Use

We strive to keep Uffizzi Cloud free or inexpensive for individuals and small teams. Therefore, activities such as crypto mining, filesharing, bots, and similar uses that lead to increased cost and intermittent issues for other users are strictly prohibited per the [Acceptable Use Policy](https://uffizzi.zendesk.com/hc/en-us/articles/4410657390999-Acceptable-Use-Policy). Violators of this policy are subject to permanent ban.

## Architecture of this Example App

The application defined by this repo allows users to vote for dogs or cats and see the results. It consists of the following microservices:  

* **voting-app** - A frontend web app in [Python](/vote) which lets you vote between two options
* **redis** - A [Redis](https://hub.docker.com/_/redis/) queue which collects new votes
* **worker** - A [.NET Core](/worker/src/Worker) worker which consumes votes and stores them in…
* **db** - A [PostgreSQL](https://hub.docker.com/_/postgres/) database backed by a Docker volume
* **result-app** - A [Node.js](/result) web app which shows the results of the voting in real time

## Set up ephemeral environments for your application

You can follow this [step-by-step guide](https://docs.uffizzi.com/set-up-uffizzi-for-your-application) to configure ephemeral environments for your own application. The required components are:

- **A Docker Compose template (`docker-compose.uffizzi.yml`) committed to your repo** - This template must include `ingress` and `services` definitions. For a full list of supported keywords, see [Docker Compose for Uffizzi](https://docs.uffizzi.com/references/compose-spec/).

- **A Uffizzi environment-action job added to your pipeline** - See our GitLab [`environment-action`](https://gitlab.com/uffizzi/environment-action) and [example workflow](https://gitlab.com/uffizzi/quickstart/-/blob/master/.gitlab-ci.yml) which will help you get started.

## FAQs
<details><summary><b>What about my database?</b></summary>
<p>All services defined by your Docker Compose file are deployed to ephemeral environments as containers—this includes databases, caches, and other datastores. This means that even if you use a managed database service like Amazon RDS for production, you should use a database <i>image</i> in your Compose (See <a href=“https://gitlab.com/uffizzi/example-voting-app/-/blob/main/docker-compose.template.yml#L13”>this example</a> that uses a <code>postgres</code> image from Docker Hub).</p>

<p>If your application requires test data, you will need to seed your database when your ephemeral environment is created. You can read about database seeding strategies on <a href="https://www.uffizzi.com/preview-environments-guide/database-seeding">our blog.</a></p>
</details>

<details><summary><b>Does Uffizzi support monorepos/polyrepos?</b></summary>
Yes. Your CI pipeline will typically include a series of <code>build</code>/<code>push</code> steps for each of the components of your application. Uffizzi just needs to know the fully qualified container registry URL for where to find these built images.
</details>

<details><summary><b>Does Uffizzi support &nbsp; _____________?</b></summary>
Uffizzi is container-centric and primarily designed for web languages. In general, if your application can be containerized, described with Docker Compose, and accepts HTTP traffic, Uffizzi can preview it.
</details>

<details><summary><b>How can my application services communicate?</b></summary>
Just like when you run <code>docker-compose up</code> locally, all the <code>services</code> defined in your Compose share a local network and can communicate via <code>localhost:port</code>. Applications that belong to different ephemeral environments may only communicate via the public Internet.
</details>

<details><summary><b>How is Uffizzi different from GitLab CI (or other CI providers)?</b></summary>
Uffizzi does not replace GitLab or any other CI provider. Uffizzi previews are meant to be added as a step in your existing CI pipeline, after your container images are built and pushed to a container registry.
</details>

<details><summary><b>Is Uffizzi open source?</b></summary>
Yes. Check out the <a href=“https://github.com/UffizziCloud/uffizzi”>main repo</a>
</details>

## Get in touch

For questions, concerns, issues, or feature requests, please join our fast growing [community](https://uffizzi.slack.com/join/shared_invite/zt-ffr4o3x0-J~0yVT6qgFV~wmGm19Ux9A#/shared-invite/email) on Slack.